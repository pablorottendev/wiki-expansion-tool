package pablorotten.wiki_expansion_tool.web;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import pablorotten.Tree.TreeNode;
import pablorotten.wiki_expansion_tool.model.Notion;
import pablorotten.wiki_expansion_tool.model.WikiExpansion;
 
@Controller
public class WikiExpansionController {
  
  @RequestMapping("/welcome")
  public ModelAndView helloWorld() {
 
    String message = "<br><div style='text-align:center;'><h3>WIKIPEDIA EXPANSION TOOL <3</h3></div><br><br>";
    return new ModelAndView("welcome", "message", message);
  } 
  
  @RequestMapping("/test1")
  public ModelAndView test() {
 
    return new ModelAndView("test1");
  } 
  
  @RequestMapping("/test2")
  public ModelAndView tests(@RequestParam String notion1, @RequestParam String notion2) {
    String notions = notion1 + " " + notion2;
    return new ModelAndView("test2", "notions", notions);
  }

  @RequestMapping("/expand")
  public ModelAndView expand(
      @RequestParam String notion1, 
      @RequestParam String notion2, 
      @RequestParam String notion3,
      @RequestParam String notion4, 
      @RequestParam String notion5
      ) throws JSONException, IOException {
    String expansionResult = "";
    WikiExpansion expansion = new WikiExpansion();
    HashMap<Notion, TreeNode<Notion>> expandedNotionTrees = new HashMap<>();
    List<String> initialTerms = Arrays.asList(notion1, notion2, notion3, notion4, notion5);

    if(initialTerms.size()<=1)
      return new ModelAndView("expand", "expansionResult", "You must insert at least 2 notions");      

    try {
      expandedNotionTrees = expansion.expand(initialTerms.stream().filter(notion -> !notion.isEmpty()).collect(Collectors.toList())); // filter out the empty notions
    } catch(Exception e) {
      System.err.println("ERRORRRRR!!!! Update number 2 " + e.getMessage());
      return new ModelAndView("error", "errorMessage", e.getMessage());
    }

    for(Entry<Notion, TreeNode<Notion>> expandedNotionTreeEntry : expandedNotionTrees.entrySet()) {
      TreeNode<Notion> expandedNotionTree = expandedNotionTreeEntry.getValue();
      Notion commonCategory = expandedNotionTreeEntry.getKey();
      expansionResult += "******************* COMMON CATEGORY: " + commonCategory.getTitle() + " *******************<br>";
      
      for (TreeNode<Notion> node : expandedNotionTree) {
        String indent = createIndent(node.getLevel());
        expansionResult += indent;
        if(node.isLeaf())
            expansionResult += "<b>" + node.data.getTitle() + "</b><br>";
        else
            expansionResult += node.data.getTitle() + "<br>";      
      }
    }  
    
    return new ModelAndView("expand", "expansionResult", expansionResult);
  }
  
  public static String createIndent(int depth) {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < depth; i++) {
      sb.append("&nbsp;&nbsp;&nbsp;&nbsp;");
    }
    return sb.toString();
  }

}